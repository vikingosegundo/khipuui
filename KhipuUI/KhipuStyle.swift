//
//  KhipuStyle.swift
//  KhipuUI
//
//  Created by Manuel Meyer on 03.09.22.
//

import SwiftUI

struct KhipuStyle {
    //typealias Color = UIKit.UIColor
    typealias Color = SwiftUI.Color
    enum Value<T> {
        case unchanged
        case unset
        case value(T)
    }
    enum Padding {
        case leading (to:Value<Double>)
        case trailing(to:Value<Double>)
        case top     (to:Value<Double>)
        case bottom  (to:Value<Double>)
        case to      (_P); enum _P {
            case paddings(
                top      : Value<Double>,
                trailing : Value<Double>,
                bottom   : Value<Double>,
                leading  : Value<Double>)
        }
    }
    enum Change {
        case setting(Setting); enum Setting {
            case size    (to:Value<(width:Double,height:Double)>)
            case position(to:Value<(    x:Double,     y:Double)>)
            case padding (Padding)
            case color(Value<Color>,for:Color.Destination)
            case alignment(to:Value<Alignment>)
            case textAlignment(to:Value<TextAlignment>)
            case cornerRadius(to:Value<Double>)
            case borderWidth(to:Value<Double>)
            case borderDashs(to:Value<[Double]>)
        }
        case execute((KhipuStyle) -> ())
    }
    
    let size         : Value<(width:Double,height:Double)>
    let position     : Value<(x:Double,y:Double)>
    let alignment    : Value<Alignment>
    let fgColor      : Value<Color>
    let bgColor      : Value<Color>
    let borderColor  : Value<Color>
    let borderWidth  : Value<Double>
    let borderDashs  : Value<[Double]>
    let paddings     : Padding
    let textAlignment: Value<TextAlignment>
    let cornerRadius : Value<Double>
    init() { self.init(
        .unchanged,
        .unchanged,
        .unchanged,
        .to(.paddings(
            top      :.unchanged,
            trailing :.unchanged,
            bottom   :.unchanged,
            leading  :.unchanged)),
        .unchanged,
        .unchanged,
        .unchanged,
        .unchanged,
        .unchanged,
        .unchanged,
        .unchanged
    )
    }
    
    init(with cs: Change...) {
        let t = KhipuStyle().alter(by:cs)
        size          = t.size
        position      = t.position
        alignment     = t.alignment
        paddings      = t.paddings
        fgColor       = t.fgColor
        bgColor       = t.bgColor
        borderColor   = t.borderColor
        cornerRadius  = t.cornerRadius
        borderWidth   = t.borderWidth
        borderDashs    = t.borderDashs
        textAlignment = t.textAlignment
    }
    
    private init(_ s:Value<(width:Double,height:Double)>,
                 _ pos:Value<(x:Double,y:Double)>,
                 _ al:Value<Alignment>,
                 _ pad: Padding,
                 _ fg:Value<Color>,
                 _ bg:Value<Color>,
                 _ bc:Value<Color>,
                 _ bw:Value<Double>,
                 _ caps:Value<[Double]>,
                 _ cr:Value<Double>,
                 _ tal:Value<TextAlignment>)
    {
        size          = s
        position      = pos
        alignment     = al
        paddings      = pad
        fgColor       = fg
        bgColor       = bg
        borderColor   = bc
        borderWidth   = bw
        borderDashs    = caps
        cornerRadius  = cr
        textAlignment = tal
    }
    
    func alter(by cs: Change...) -> Self { cs.reduce(self){ $0.alter($1) } }
    func alter(by cs:[Change]  ) -> Self { cs.reduce(self){ $0.alter($1) } }
    private
    func alter(_ c:Change) -> Self {
        switch c {
        case let .execute(fun): fun(self); return self
        case let .setting(.position( to:p)                       ): return Self(size, p       ,alignment,paddings,fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.size    ( to:s)                       ): return Self(s,    position,alignment,paddings,fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.padding (.to(.paddings(top     : top,
                                                  trailing: trail,
                                                  bottom  : bott,
                                                  leading : lead))))
                                                                  : return Self(size,position,alignment,.to(.paddings(top:top,trailing:trail,bottom:bott,leading:lead)),fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case     .setting(.padding ( to:.leading (to:.unchanged))): return self
        case     .setting(.padding ( to:.trailing(to:.unchanged))): return self
        case     .setting(.padding ( to:.top     (to:.unchanged))): return self
        case     .setting(.padding ( to:.bottom  (to:.unchanged))): return self
        case let .setting(.padding ( to:.leading (to:lead  )    )): return Self(size,position,alignment,.to(.paddings(top:extractTop(from:paddings),trailing:extractTrailing(from:paddings),bottom:extractBottom(from:paddings),leading:lead                         )),fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.padding ( to:.trailing(to:trail )    )): return Self(size,position,alignment,.to(.paddings(top:extractTop(from:paddings),trailing:trail                         ,bottom:extractBottom(from:paddings),leading:extractLeading(from:paddings))),fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.padding ( to:.top     (to:top   )    )): return Self(size,position,alignment,.to(.paddings(top:top                      ,trailing:extractTrailing(from:paddings),bottom:extractBottom(from:paddings),leading:extractLeading(from:paddings))),fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.padding ( to:.bottom  (to:bottom)    )): return Self(size,position,alignment,.to(.paddings(top:extractTop(from:paddings),trailing:extractTrailing(from:paddings),bottom:bottom                      ,leading:extractLeading(from:paddings))),fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case     .setting(.color   ( .unchanged, for:_          )): return self
        case let .setting(.color   ( c,          for:.foreground)): return Self(size,position,alignment,paddings,c      ,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.color   ( c,          for:.background)): return Self(size,position,alignment,paddings,fgColor,c      ,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.color   ( c,          for:.border    )): return Self(size,position,alignment,paddings,fgColor,bgColor,c          ,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.borderWidth  (to:bw                  )): return Self(size,position,alignment,paddings,fgColor,bgColor,borderColor,bw         ,borderDashs,cornerRadius,textAlignment)
        case let .setting(.borderDashs  (to:dashs               )): return Self(size,position,alignment,paddings,fgColor,bgColor,borderColor,borderWidth,dashs      ,cornerRadius,textAlignment)
        case let .setting(.cornerRadius (to:cr                  )): return Self(size,position,alignment,paddings,fgColor,bgColor,borderColor,borderWidth,borderDashs,cr          ,textAlignment)
        case let .setting(.textAlignment(to:textAlignment       )): return Self(size,position,alignment,paddings,fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        case let .setting(.alignment    (to:alignment           )): return Self(size,position,alignment,paddings,fgColor,bgColor,borderColor,borderWidth,borderDashs,cornerRadius,textAlignment)
        }
    }
}

extension KhipuStyle.Color {
    enum Destination { case foreground, background, border }
}

fileprivate func extractTop(from pad:KhipuStyle.Padding) -> KhipuStyle.Value<Double> {
    switch pad {
    case let .to(.paddings(top: top, trailing: _, bottom: _, leading: _)) : return top
    case let .top(to: top): return top
    default : return .unchanged
    }
}
fileprivate func extractTrailing(from pad:KhipuStyle.Padding) -> KhipuStyle.Value<Double> {
    switch pad {
    case let .to(.paddings(top: _, trailing: trail, bottom: _, leading: _)) : return trail
    case let .trailing(to: trail): return trail
    default : return .unchanged
    }
}
fileprivate func extractBottom(from pad:KhipuStyle.Padding) -> KhipuStyle.Value<Double> {
    switch pad {
    case let .to(.paddings(top: _, trailing:_, bottom:bottom, leading: _)) : return bottom
    case let .bottom  (to : bottom): return bottom
    default : return .unchanged
    }
}
fileprivate func extractLeading(from pad:KhipuStyle.Padding) -> KhipuStyle.Value<Double> {
    switch pad {
    case let .to(.paddings(top: _, trailing:_, bottom:_, leading: lead)) : return lead
    case let .leading(to : lead): return lead
    default : return .unchanged
    }
}
