//
//  KhipuView.swift
//  KhipuUI
//
//  Created by Manuel Meyer on 03.09.22.
//

import SwiftUI

struct KhipuView: View {
    var body: some View {
        VStack {
            Text("Hey Ho! Let's go!")
                .alter(
                    by:
                        .setting(
                            .padding(.to(.paddings(
                                top      : .value(20),
                                trailing : .value(20),
                                bottom   : .value(20),
                                leading  : .value(20))))),
                        .setting(.padding(.leading (to:.value(90)))),
                        .setting(.padding(.trailing(to:.value(30)))),
                        .setting(.textAlignment(to:.value(.trailing))),
                        .setting(.color(.value(.indigo),for:.background)),
                        .setting(.color(.value(.white ),for:.foreground)),
                        .setting(.color(.value(.mint  ),for:.border)),
                        .setting(.cornerRadius(to:.value(35))),
                        .setting(.borderWidth (to:.value( 4))),
                        .execute({ print($0) }))
            Image("m1")
                .alter(
                    by:
                        .setting(.borderWidth(to:.value(4))),
                        .setting(.color(.value(.teal),for:.border)),
                        .setting(.cornerRadius(to:.value(50))),
                        .execute({ print($0) })
            )
            Button{ print("Hey Ho!\nLet's go!") } label: {
                Spacer()
                VStack {
                    Spacer()
                    HStack {
                        Text("Hey Ho!\nLet's go! — Blitzkrieg Bob")
                    }
                    Spacer()
                }
                Spacer()
            }.contentShape(RoundedRectangle(cornerRadius:  30))
            .alter(
                by:
                    //.setting(.padding(.to(.paddings(
                    //    top      : .value(24),
                    //    trailing : .value(24),
                    //    bottom   : .value(24),
                    //    leading  : .value(24))))),
                    .setting(.size(to:.value((width:230, height:130)))),
                    .setting(.textAlignment(to:.value(.center))),
                    .setting(.color(.value(.gray ),for:.background)),
                    .setting(.color(.value(.white),for:.foreground)),
                    .setting(.color(.value(.blue ),for:.border    )),
                    .setting(.cornerRadius(to:.value(30))),
                    .setting(.borderWidth (to:.value(2))),
                    .setting(.borderDashs (to:.value([4,1]))),
                    .setting(.alignment(to:.value(.center))),
                    .execute({ print($0) })
            )
        }
    }
}

struct StyledKhipuView: ViewModifier {
    let size           : (width:Double,height:Double)?
    let alignment      : Alignment?
    let paddingTop     : Double?
    let paddingTrailing: Double?
    let paddingBottom  : Double?
    let paddingLeading : Double?
    let bgColor        : Color?
    let fgColor        : Color?
    let borderColor    : Color?
    let borderWidth    : Double?
    let cornerRadius   : Double?
    let textAlignment  : TextAlignment?
    let dash           : [Double]
    func body(content: Content) -> some View {
        HStack {
            content
                .padding(.top     , paddingTop      ?? 0.0)
                .padding(.trailing, paddingTrailing ?? 0.0)
                .padding(.bottom  , paddingBottom   ?? 0.0)
                .padding(.leading , paddingLeading  ?? 0.0)
                .frame(width:size?.width != nil ? CGFloat(size!.width) : nil, height:size?.height != nil ? CGFloat(size!.height) : nil)
                .background(RoundedRectangle(cornerRadius: cornerRadius ?? 0.0).foregroundColor(bgColor ?? .clear))
                .foregroundColor(fgColor)
                .multilineTextAlignment(textAlignment ?? .leading)
                .cornerRadius(cornerRadius ?? 0.0)
                .overlay(
                    RoundedRectangle(cornerRadius:cornerRadius ?? 0.0)
                        .stroke(borderColor ?? .clear,
                                style:StrokeStyle(lineWidth:borderWidth ?? 1.0, dash:dash.map{CGFloat.init($0)})))
        }
        
    }
}
extension View {
    func alter (by cs:KhipuStyle.Change...) -> some View {
        let style = KhipuStyle().alter(by: cs)
        return alter(style)
    }
    func alter(_ style:KhipuStyle) -> some View {
        let leading,trailing,top,bottom:Double?
        let bg,fg,bc: Color?
        let size: (width:Double, height:Double)?
        let alignment: Alignment?
        let textAlignment: TextAlignment?
        let cornerRadius: Double?
        let borderWidth: Double?
        let dashes: [Double]
        switch style.paddings {
        case let .to(.paddings(top:to,trailing:trail,bottom:bott,leading:lead)):
            switch to {
            case .unchanged: top = nil
            case .unset: top = 0
            case .value(let t): top = t
            }
            switch bott {
            case .unchanged: bottom = nil
            case .unset: bottom = 0
            case .value(let t): bottom = t
            }
            switch lead {
            case .unchanged: leading = nil
            case .unset: leading = 0
            case .value(let t): leading = t
            }
            switch trail {
            case .unchanged: trailing = nil
            case .unset: trailing = 0
            case .value(let t): trailing = t
            }
        case .bottom(to: let bot):
            switch bot {
            case .unchanged: bottom = nil
            case .unset: bottom = 0
            case .value(let v): bottom = v
            }
            leading = nil
            trailing = nil
            top = nil
            
        case .top(to: let t):
            switch t {
            case .unchanged: top = nil
            case .unset: top = 0
            case .value(let v): top = v
            }
            leading = nil
            trailing = nil
            bottom = nil
        case .trailing(to: let t):
            switch t {
            case .unchanged: trailing = nil
            case .unset: trailing = 0
            case .value(let v): trailing = v
            }
            leading = nil
            top = nil
            bottom = nil
        case .leading(to: let l):
            switch l {
            case .unchanged   : leading = nil
            case .unset       : leading = 0
            case .value(let v): leading = v
            }
            top = nil
            trailing = nil
            bottom = nil
        }
        switch style.bgColor {
        case .value(let c): bg = c
        case .unset: bg = nil
        case .unchanged: bg = nil
        }
        switch style.fgColor {
        case .value(let c): fg = c
        case .unset: fg = nil
        case .unchanged: fg = nil
        }
        switch style.borderColor {
        case .value(let s): bc = s
        case .unchanged: bc = nil
        case .unset: bc = nil
        }
        switch style.borderWidth {
        case .value(let s): borderWidth = s
        case .unchanged: borderWidth = nil
        case .unset: borderWidth = nil
        }
        
        switch style.size {
        case .value(let s): size = s
        case .unchanged: size = nil
        case .unset: size = (0,0)
        }
        switch style.alignment {
        case .value(let s): alignment = s
        case .unchanged: alignment = nil
        case .unset: alignment = nil
        }
        switch style.textAlignment {
        case .value(let s): textAlignment = s
        case .unchanged: textAlignment = nil
        case .unset: textAlignment = nil
        }
        switch style.cornerRadius {
        case .value(let s): cornerRadius = s
        case .unchanged: cornerRadius = nil
        case .unset: cornerRadius = nil
        }
        switch style.borderDashs {
        case .value(let s): dashes = s
        case .unchanged: dashes = []
        case .unset: dashes = []
        }
        return modifier(
            StyledKhipuView(
                size            : size,
                alignment       : alignment,
                paddingTop      : top,
                paddingTrailing : trailing,
                paddingBottom   : bottom,
                paddingLeading  : leading,
                bgColor         : bg,
                fgColor         : fg,
                borderColor     : bc,
                borderWidth     : borderWidth,
                cornerRadius    : cornerRadius,
                textAlignment   : textAlignment,
                dash            : dashes
            )
        )
    }
}

struct KhipuView_Previews: PreviewProvider {
    static var previews: some View {
        KhipuView()
    }
}
