//
//  KhipuUIApp.swift
//  KhipuUI
//
//  Created by Manuel Meyer on 02.09.22.
//

import SwiftUI

@main
struct KhipuUIApp: App {
    var body: some Scene {
        WindowGroup {
            KhipuView()
        }
    }
}
